set nocompatible
filetype off

call plug#begin()
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/vim-slash'
Plug 'tpope/vim-bundler'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/gv.vim'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-rails'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'michaeljsmith/vim-indent-object'
Plug 'vim-scripts/argtextobj.vim'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'vim-ruby/vim-ruby'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'wellle/targets.vim'
" Plug 'rstacruz/sparkup'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
" Plug 'vim-syntastic/syntastic'
Plug 'kana/vim-textobj-user'
Plug 'nelstrom/vim-textobj-rubyblock'
Plug 'vimwiki/vimwiki'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
call plug#end()

let g:fzf_command_prefix = 'Fzf'

set hidden
set vb t_vb=
set ts=2 sts=2 sw=2 expandtab
set autoindent
set backspace=indent,eol,start
set complete-=i
set smarttab

" disable fonlding for markdown
let g:vim_markdown_folding_disabled = 1

" change mapping for next in sparkup
let g:sparkupNextMapping = '<c-m>'

" vim-javascript
let g:javascript_plugin_flow = 1

" " syntastic recommended defaults
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0

" current file directory
nnoremap <space>- :FZF <c-r>=fnameescape(expand('%:p:h'))<cr>/<cr>
" current working directory
nnoremap <space>+ :FZF<cr>
nnoremap <space>b :FzfBuffers<cr>
nnoremap <space>ff :FZF<space>
nnoremap <space>fl :FzfLines<cr>
nnoremap <space>fb :FzfBLines<cr>
nnoremap <space>ft :FzfTags<cr>
nnoremap <space>fr :FzfHistory<cr>
nnoremap <space>fh :FzfHelptags<cr>
nnoremap <space>f: :FzfHistory:<cr>
nnoremap <space>f/ :FzfHistory/<cr>
nnoremap <space>fg :FzfGFiles<cr>
nnoremap <space>fs :FzfGFiles?<cr>
nnoremap <space>fc :FzfCommits<cr>
nnoremap <space><space> :FzfCommands<cr>

set number relativenumber

" will temporatily disable arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" Maps <C-> h,j,k,l to move between windows
noremap <Esc>j <C-w>j
noremap <Esc>k <C-w>k
noremap <Esc>h <C-w>h
noremap <Esc>l <C-w>l

" mapleader configuration
let mapleader = ","

" some of the maps taken from:
" https://github.com/r00k/dotfiles/blob/master/vimrc
map <Leader>d orequire 'pry'<cr>binding.pry<esc>:w<cr>
map <Leader>m :Rmodel

" fugitive git bindings
nnoremap <Leader>ga :Git add %:p<CR><CR>
nnoremap <Leader>gs :Gstatus<CR>
nnoremap <Leader>gc :Gcommit -v -q<CR>
nnoremap <Leader>gt :Gcommit -v -q %:p<CR>
nnoremap <Leader>gd :Gdiff<CR>
nnoremap <Leader>ge :Gedit<CR>
nnoremap <Leader>gr :Gread<CR>
nnoremap <Leader>gw :Gwrite<CR><CR>
nnoremap <Leader>gl :silent! Glog<CR>:bot copen<CR>
nnoremap <Leader>gp :Ggrep<Space>
nnoremap <Leader>gm :Gmove<Space>
nnoremap <Leader>gb :Git branch<Space>
nnoremap <Leader>go :Git checkout<Space>
nnoremap <Leader>gv :GV<CR>
nnoremap <Leader>gps :Dispatch! git push<CR>
nnoremap <Leader>gpl :Dispatch! git pull<CR>

" comments for todos, fixme and note shortcuts
map <Leader>ct oTODO <esc>gccA
map <Leader>cf oFIXME <esc>gccA
map <Leader>cn oNOTE <esc>gccA

" reselect, indent and de-indent recently pasted block
nnoremap <Leader>p `[V`]
nnoremap <Leader>[ `[V`]<
nnoremap <Leader>] `[V`]>

" Git gutter settings
nmap ]h <Plug>(GitGutterNextHunk)
nmap [h <Plug>(GitGutterPrevHunk)

" Tabularize mappings
nmap <Leader>a= :Tabularize /=<CR>
vmap <Leader>a= :Tabularize /=<CR>
nmap <Leader>a: :Tabularize /:\zs<CR>
vmap <Leader>a: :Tabularize /:\zs<CR>
nmap <Leader>a\| :Tabularize /<Bar><CR>
vmap <Leader>a\| :Tabularize /<Bar><CR>

" Safari reload via osascript
nnoremap <Leader>sr :w\|:!osascript -e 'tell app "Safari" to do JavaScript "window.location.reload();" in current tab of window 1'<cr>
nnoremap <Leader>cr :w\|:!chrome-cli reload<cr>

" Note that remapping C-s requires flow control to be disabled
" (e.g. in .bashrc or .zshrc)
map <C-s> <esc>:w<CR>
imap <C-s> <esc>:w<CR>

" For nerdtree
map <C-n> :NERDTreeToggle<CR>

" For running ctags
nnoremap <f5> :!ctags -R --exclude=.git --exclude=log --exclude=node_modules --exclude=public/javascripts *<CR>

" Uses ack
set grepprg=ack\ --nogroup\ --column\ $*
set grepformat=%f:%l:%c:%m

" matchit
packadd! matchit
